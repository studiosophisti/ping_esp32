from __future__ import absolute_import, unicode_literals
import os
import random
import string
import sys
from Crypto import Random
from Crypto.Cipher import AES

AES_BLOCK_SIZE = 16
RSA_KEY_SIZE = 64 # must be 16 fold

def main(argv):   

    rsa_key = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits)
                  for _ in range(RSA_KEY_SIZE))
    print rsa_key

    salt = argv[0]

    iv = Random.new().read(AES_BLOCK_SIZE)
    cipher = AES.new(salt, AES.MODE_CBC, iv)
    encrypted_key = cipher.encrypt(rsa_key)

    with open('data/key', 'w') as fileOut:
        fileOut.write(iv)
        fileOut.write(encrypted_key)
        fileOut.close()

if __name__ == "__main__":
    main(sys.argv[1:])