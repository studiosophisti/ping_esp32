import os
import shutil
import re

with open("stack.txt") as fileIn:	
    for line in fileIn:
    	for part in line.split(' '):
	        match = re.search('40[0-2](\\d|[a-f]){5}\\b', part)
	        if match:
	            addr = match.group(0)
	            print addr
	            os.system("xtensa-esp32-elf-addr2line -aipfC -e ../build/Minibrew.elf " + addr)

    fileIn.close();
