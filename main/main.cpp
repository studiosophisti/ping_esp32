//ESP32 programming guide: https://esp-idf.readthedocs.io/en/v1.0/index.html
//ESP32 Arduino SDK: https://github.com/espressif/arduino-esp32

#include "Arduino.h"
#include "IOControl.h"
#include "PingBLEService.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

xQueueHandle gpio_evt_queue = NULL;

void task_io_control( void* p )
{
    uint32_t interval = 100;

    while( true )
    {
    	delay(interval);
    	IOControl::sharedInstance()->setNeedsUpdate();
    	IOControl::sharedInstance()->loop();
    }
}

void task_ble_scan( void* p )
{
	/* If gravity sensor is paired, wait half a minute before we start scanning to connect to the broker
	if (IOControl::sharedInstance()->getGravitySensor()->isPaired()) {
		delay(30000);
	}*/
    for(;;) {
//    		IOControl::sharedInstance()->getGravitySensor()->loop();
    		delay(100);
    }
}


void setup(){

	Serial.begin(115200);

	Serial.println("Hello Ping");

	gpio_evt_queue = xQueueCreate(10, sizeof(uint32_t));

    // gpio32 route to digital io_mux
    REG_CLR_BIT(RTC_IO_XTAL_32K_PAD_REG, RTC_IO_X32P_MUX_SEL);

    // gpio33 route to digital io_mux
    REG_CLR_BIT(RTC_IO_XTAL_32K_PAD_REG, RTC_IO_X32N_MUX_SEL);

    // talk to the IO controller
    IOControl::sharedInstance()->setNeedsUpdate();
    IOControl::sharedInstance()->loop();

    PingBLEService::sharedInstance()->begin();

    xTaskCreate(&task_io_control, "task_io_control", 4096, NULL, 10, NULL);
}


void loop(){

    if (PingBLEService::sharedInstance()->isConnected()) {
    		PingBLEService::sharedInstance()->loop();
    }

    //this delay is vital for connections stability, don't remove.
    delay(100);
}
