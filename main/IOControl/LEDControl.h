/*
 * LEDControl.h
 *
 *  Created on: Dec 11, 2018
 *      Author: Imara Sophisti
 */

#ifndef MAIN_LEDCONTROL_H_
#define MAIN_LEDCONTROL_H_

#include <stdio.h>
#include "Constants.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/ledc.h"
#include "esp_err.h"

#define LEDC_HS_TIMER          	LEDC_TIMER_0
#define LEDC_HS_MODE           	LEDC_HIGH_SPEED_MODE
#define LED_A					0
#define LED_B					1
#define LED_C					2
#define LEDC_HS_CH0_GPIO       	(GPIO_LED0)
#define LEDC_HS_CH0_CHANNEL    	LEDC_CHANNEL_0
#define LEDC_HS_CH1_GPIO       	(GPIO_LED1)
#define LEDC_HS_CH1_CHANNEL    	LEDC_CHANNEL_1
#define LEDC_LS_CH2_GPIO       	(GPIO_LED2)
#define LEDC_LS_CH2_CHANNEL    	LEDC_CHANNEL_2

#define LEDC_FREQUENCY			(20000)
#define LEDC_DUTY_RES			LEDC_TIMER_11_BIT
#define LEDC_DUTY_MAX			(2047)	// max in 11 bits
#define LEDC_BRIGHT_STEPS		(0.2f)

#define LEDC_TEST_CH_NUM       (3)
#define LEDC_TEST_DUTY         (2000)
#define LEDC_TEST_FADE_TIME    (3000)

struct LedColor {
	float a = 0;
	float b = 0;
	float c = 0;

	LedColor() {
		LedColor(0, 0, 0);
	}

	LedColor(float A, float B, float C) {
		a = A;
		b = B;
		c = C;
	}
};

class LEDControl {

public:
	LEDControl();

	void setLedABC(LedColor RGBWcolor, float t);
	void setLedABC(float A, float B, float C, float t);

	void setMAX(float MAX) { if (MAX >= 0 && MAX <= 1) {lightCAP = MAX;}; }

	float getLightCAP() { return lightCAP; }

private:

	float lightCAP, prevLightCap;

	float led_A, led_B, led_C;

	ledc_timer_config_t ledc_timer;
	ledc_channel_config_t ledc_channel[LEDC_TEST_CH_NUM];

};

#endif /* MAIN_LEDCONTROL_H_ */
