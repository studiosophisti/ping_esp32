//
//  IOControl.h
//  MiniBrewWifi
//
//  Created by Tijn Kooijmans on 19/02/16.
//  Copyright © 2016 Studio Sophisti. All rights reserved.
//

#ifndef IOControl_hpp
#define IOControl_hpp

#include "Constants.h"
#include "LEDControl.h"

class IOControl {

	enum statusLed{
		OFF,
		HALF,
		FULL
	};

public:

	static IOControl* sharedInstance();

	// called by main application loop, updates the I/O if flag is set
	void loop();

	void updateLeds();
	void setLeds(statusLed status);

	// set flag to update I/O at next loop
	void setNeedsUpdate() { _needsUpdateFlag = true; }

	void setPeerIsOn(bool on) { _peerIsOn = on; updateLeds(); }

private:
	static IOControl *singleton;

	IOControl();

	bool _needsUpdateFlag;
	bool _peerIsOn;
	bool _iAmOn;

	LEDControl _leds;
};

#endif /* IOControl_hpp */
