//
//  IOControl.cpp
//  MiniBrewWifi
//
//  Created by Tijn Kooijmans on 19/02/16.
//  Copyright © 2016 Studio Sophisti. All rights reserved.
//

#include "IOControl.h"
#include <algorithm>
#include "esp_sleep.h"
#include "pins_arduino.h"
#include "../PingBLEService.h"

// interrupt variables
volatile bool vibrated = false;
volatile uint32_t vibrateTime = 0;

IOControl* IOControl::singleton = nullptr;

IOControl* IOControl::sharedInstance()
{
    if(singleton == nullptr)
        singleton = new IOControl();

    return singleton;
}

// interrupt attached to the vibration sensor
void IRAM_ATTR vibrationTrigger() {

	if (millis() - vibrateTime > DEBOUNCE_VIB)
	{
		vibrateTime = millis();
		vibrated = true;			// if vibrated is true, Ping should turn off
	}
}

IOControl::IOControl() {

    Serial.println("Initializing IO Control");

    // initialise variables
    _needsUpdateFlag = false;
    _peerIsOn = false;
    _iAmOn = false;

	_leds.setLedABC(0, 0, 0, 0);
	updateLeds();

    pinMode(GPIO_VIBG, OUTPUT);
    digitalWrite(GPIO_VIBG, HIGH);	// ground 1 pin of vibration interrupt TODO change this to fit vibration

    pinMode(GPIO_VIB, INPUT_PULLDOWN);
    attachInterrupt(digitalPinToInterrupt(GPIO_VIB), vibrationTrigger, FALLING);		// TODO do we need to set the VIB pin as an interrupt? Is recommended

    Serial.println("Initializing IO Control complete");
}

void IOControl::loop() {

    if (_needsUpdateFlag) {
        _needsUpdateFlag = false;

        if (vibrated)
        {
        	Serial.println("Ping vibrated");
        	vibrated = false;

        	_iAmOn = !_iAmOn;

        	uint8_t buf[1];
        	if (_iAmOn) {
        		buf[0] = 1;
        		PingBLEService::sharedInstance()->sendData(buf, 1);
        	} else {
        		buf[0] = 0;
        		PingBLEService::sharedInstance()->sendData(buf, 1);
        	}

        	updateLeds();
        }
    }
}

void IOControl::updateLeds() {
	if (_iAmOn) {
		if (_peerIsOn) {
			setLeds(FULL);
		} else {
			setLeds(HALF);
		}
	} else {
		if (_peerIsOn) {
			setLeds(HALF);
		} else {
			setLeds(OFF);
		}
	}
}

void IOControl::setLeds(statusLed status) {

	if (status == OFF)
	{
		_leds.setLedABC(0, 0, 0, 400);
	}
	else if (status == HALF)
	{
		_leds.setLedABC(0.1, 0.1, 0.1, 400);
	}
	else if (status == FULL)
	{
		_leds.setLedABC(1.0, 1.0, 1.0, 400);
	}
}






