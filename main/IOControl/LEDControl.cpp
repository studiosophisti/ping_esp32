/*
 * LEDControl.cpp
 *
 *  Created on: Dec 11, 2018
 *      Author: Imara Sophisti
 */

#include "LEDControl.h"

LEDControl::LEDControl() {
	// Prepare and set configuration of timers that will be used by LED Controller

	// when working with 20KHz, we can only use 11 bits resolution
	ledc_timer.duty_resolution = LEDC_DUTY_RES;  	// resolution of PWM duty
	ledc_timer.freq_hz = LEDC_FREQUENCY;	                      	// frequency of PWM signal
	ledc_timer.speed_mode = LEDC_HS_MODE;	           	// timer mode
	ledc_timer.timer_num = LEDC_HS_TIMER;            	// timer index

	// Set configuration of timer0 for high speed channels
	ledc_timer_config(&ledc_timer);

	/*
	 * Prepare individual configuration
	 * for each channel of LED Controller
	 * by selecting:
	 * - controller's channel number
	 * - output duty cycle, set initially to 0
	 * - GPIO number where LED is connected to
	 * - speed mode, either high or low
	 * - timer servicing selected channel
	 *   Note: if different channels use one timer,
	 *         then frequency and bit_num of these channels
	 *         will be the same
	 */

	for (int ch = 0; ch < LEDC_TEST_CH_NUM; ch++) {
		ledc_channel[ch].duty = 0;
		ledc_channel[ch].speed_mode = LEDC_HS_MODE;
		ledc_channel[ch].hpoint = 0;
		ledc_channel[ch].timer_sel = LEDC_HS_TIMER;
	}

	ledc_channel[0].channel    = LEDC_HS_CH0_CHANNEL;
	ledc_channel[0].gpio_num   = LEDC_HS_CH0_GPIO;

	ledc_channel[1].channel    = LEDC_HS_CH1_CHANNEL;
	ledc_channel[1].gpio_num   = LEDC_HS_CH1_GPIO;

	ledc_channel[2].channel    = LEDC_LS_CH2_CHANNEL;
	ledc_channel[2].gpio_num   = LEDC_LS_CH2_GPIO;

	// set all variables
	lightCAP = prevLightCap = 0.4;
	led_A = 0;
	led_B = 0;
	led_C = 0;

	// Set LED Controller with previously prepared configuration
	for (int ch = 0; ch < LEDC_TEST_CH_NUM; ch++) {
		ledc_channel_config(&ledc_channel[ch]);
	}

	// Initialize fade service.
	ledc_fade_func_install(0);

	for (int ch = 0; ch < LEDC_TEST_CH_NUM; ch++) {
		// turn all leds off
		ledc_set_duty_and_update(ledc_channel[ch].speed_mode, ledc_channel[ch].channel, 0, 0);	// TODO still need to make sure that hpoint can be 0
	}


	//	uint32_t hpoint;
	//	hpoint = ledc_get_hpoint(ledc_channel[0].speed_mode, ledc_channel[0].channel);
	//	printf("hpoint: %d", hpoint);

}


void LEDControl::setLedABC(LedColor color, float t) {
	setLedABC(color.a, color.b, color.c, t);
}

void LEDControl::setLedABC(float A, float B, float C, float t) {

	if (t == 0)
	{
		if ((A != led_A || prevLightCap != lightCAP) && A >= 0 && A <= 1)
		{
			ledc_set_duty_and_update(ledc_channel[LED_A].speed_mode, ledc_channel[LED_A].channel, (A * LEDC_DUTY_MAX * lightCAP), 0);
			led_A = A;
		}
		if ((B != led_B || prevLightCap != lightCAP) && B >= 0 && B <= 1)
		{
			ledc_set_duty_and_update(ledc_channel[LED_B].speed_mode, ledc_channel[LED_B].channel, (B * LEDC_DUTY_MAX * lightCAP), 0);
			led_B = B;
		}
		if ((C != led_C || prevLightCap != lightCAP) && C >= 0 && C <= 1)
		{
			ledc_set_duty_and_update(ledc_channel[LED_C].speed_mode, ledc_channel[LED_C].channel, (C * LEDC_DUTY_MAX * lightCAP), 0);
			led_C = C;
		}
	}
	else
	{
		if (A != led_A && A >= 0 && A <= 1)
		{
			ledc_set_fade_time_and_start(ledc_channel[LED_A].speed_mode, ledc_channel[LED_A].channel, (A * LEDC_DUTY_MAX * lightCAP), t, LEDC_FADE_NO_WAIT);
			led_A = A;
		}
		if (B != led_B && B >= 0 && B <= 1)
		{
			ledc_set_fade_time_and_start(ledc_channel[LED_B].speed_mode, ledc_channel[LED_B].channel, (B * LEDC_DUTY_MAX * lightCAP), t, LEDC_FADE_NO_WAIT);
			led_B = B;
		}
		if (C != led_C && C >= 0 && C <= 1)
		{
			ledc_set_fade_time_and_start(ledc_channel[LED_C].speed_mode, ledc_channel[LED_C].channel, (C * LEDC_DUTY_MAX * lightCAP), t, LEDC_FADE_NO_WAIT);
			led_C = C;
		}
	}

	prevLightCap = lightCAP;
}
