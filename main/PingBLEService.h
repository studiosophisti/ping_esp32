/*
 * MBBLEService.h
 *
 *  Created on: 11 Sep 2017
 *      Author: tijn
 */

#ifndef MAIN_PINGBLESERVICE_H_
#define MAIN_PINGBLESERVICE_H_

#include "Constants.h"

#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>

#define BLE_BUFFER_SIZE	768

typedef enum {
	messageTypeAck = 0,
	messageTypeNack = 1,
	messageTypeLogs = 2,
	messageTypeConsole = 3,
	messageTypeStatus = 4,
	messageTypeSettings = 5,
	messageTypeDebug = 6,
	messageTypeOta = 7,
	messageTypeOtaIo = 8,
	messageTypeMqttEmulate = 9
} bleMessageType_t;

class PingBLEService {
public:
    static PingBLEService* sharedInstance();

	PingBLEService();

	void begin(String id = "");
	void sendData(uint8_t *data, size_t length);
	bool isConnected();
	bool isInit() { return _init; };
	void loop();
	void didDisconnect();
	void didConnect();
	void didReceiveData(uint8_t *data, size_t length);
	void sendAck(bool success);

private:
    static PingBLEService *singleton;

    const String bleID = "Ping";

    BLEDevice ble;
    BLEServer *_server;
    BLEService *_service;
    BLECharacteristic *_characteristic;

    bool _deviceInit;
    bool _init;
    bool _serverConnected;
    bool _rxInProgress;
    uint16_t _rxCount;
    uint16_t _rxLength;
    uint16_t _sequence;
    bleMessageType_t _rxType;
    uint8_t _rxData[BLE_BUFFER_SIZE];
    uint8_t _txData[BLE_BUFFER_SIZE];
};

#endif /* MAIN_PINGBLESERVICE_H_ */
