//
//  Constants.h
//  MiniBrewWifi
//
//  Created by Tijn Kooijmans on 19/02/16.
//  Copyright © 2016 Studio Sophisti. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#include "Arduino.h"

// pin defines
#define GPIO_LED0	4
#define GPIO_LED1	16
#define GPIO_LED2	17
#define GPIO_VIBG	12
#define GPIO_VIB	13

// timings
#define DEBOUNCE_VIB 	1000

// ble constants
#define SERVICE_UUID        "1d86a32c-d9d0-4ca2-81bd-6db7e102c593"
#define CHARACTERISTIC_UUID "f26da3f6-d9d0-4ca2-81bd-6db7e102c593"



#endif /* Constants_h */
