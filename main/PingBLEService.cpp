/*
 * MBBLEService.cpp
 *
 *  Created on: 11 Sep 2017
 *      Author: tijn
 */

#include "PingBLEService.h"
#include <vector>
#include <BLEDevice.h>
#include <BLEServer.h>
#include <string>
#include "IOControl/IOControl.h"

#define SOP_BYTE  	0x8e

class ServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
    	Serial.println("BLE Connected");
    	PingBLEService::sharedInstance()->didConnect();
    };

    void onDisconnect(BLEServer* pServer) {
    	Serial.println("BLE Disconnected");
    	PingBLEService::sharedInstance()->didDisconnect();
    }
};

class CharacteristicCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
    	BLEValue value = pCharacteristic->getValue();
    	PingBLEService::sharedInstance()->didReceiveData(value.getData(), value.getLength());
    }
};

PingBLEService* PingBLEService::singleton = nullptr;

PingBLEService* PingBLEService::sharedInstance()
{
    if(singleton == nullptr)
        singleton = new PingBLEService();

    return singleton;
}

PingBLEService::PingBLEService() {
	_server = nullptr;
	_characteristic = nullptr;
	_service = nullptr;
	_rxInProgress = false;
	_rxLength = 0;
	_serverConnected = false;
	_rxCount = 0;
	_init = false;
	_deviceInit = false;
	_rxType = messageTypeAck;
	_sequence = 0;
}

void PingBLEService::begin(String id) {
	Serial.println("Initializing BLE service");

	BLEDevice::init(bleID.c_str());	// give the device name

	// Create the BLE Server

	_server = BLEDevice::createServer();
	_server->setCallbacks(new ServerCallbacks());

	// Create the BLE Service
	_service = _server->createService(SERVICE_UUID);

	// Create a BLE Characteristic
	_characteristic = _service->createCharacteristic(
					  CHARACTERISTIC_UUID,
					  BLECharacteristic::PROPERTY_READ   |
					  BLECharacteristic::PROPERTY_WRITE  |
					  BLECharacteristic::PROPERTY_WRITE_NR  |
					  BLECharacteristic::PROPERTY_NOTIFY  |
					  BLECharacteristic::PROPERTY_INDICATE
					);
	_characteristic->setCallbacks(new CharacteristicCallbacks());

	// Start the service
	_service->start();

	 // Start advertising
	if (id.length() == 4) {
		char uuid[37];
		uuid[36] = '\0';
		memcpy(uuid, SERVICE_UUID, 36);
		memcpy(uuid + 9, id.c_str(), 4);
		_server->getAdvertising()->addServiceUUID((const char*)uuid);
	} else {
		_server->getAdvertising()->addServiceUUID(SERVICE_UUID);
	}
	_server->getAdvertising()->start();

	_init = true;
}

void PingBLEService::sendData(uint8_t *data, size_t length) {
	_characteristic->setValue(data, length);

	if (!isConnected()) return;

	_characteristic->indicate();

//	if (length + 6 > BLE_BUFFER_SIZE)
//	{
////		MBLOG("BLE TX message too large %d>%d", length, BLE_BUFFER_SIZE);
//		Serial.println("BLE TX too large");
//		return;
//	}
//
//    //SOP,SEQ,SEQ,TYPE,LEN,LEN,DATA0...DATAN
//
//	_txData[0] = SOP_BYTE;
//	_txData[1] = (sequence >> 8) & 0xFF;
//	_txData[2] = sequence & 0xFF;
//	_txData[3] = type;
//	_txData[4] = (length >> 8) & 0xFF;
//	_txData[5] = length & 0xFF;
//	memcpy(&_txData[6], data, length);
//
//	size_t txSize = length + 6;
//
//	int bytesSend = 0;
//	int mtu = BLEDevice::getMTU() - 3;
//
//    //MBLOG("BLE TX %d bytes of data", txSize);
//
//	//send in chunks of MTU size
//	while (bytesSend < txSize) {
//		if (!isConnected()) return;
//		_characteristic->setValue(_txData + bytesSend, (size_t)std::min((int)(txSize - bytesSend), mtu));
//		_characteristic->indicate();
//		bytesSend += mtu;
//	}
}

void PingBLEService::didDisconnect()
{
	_serverConnected = false;
	_rxInProgress = false;
	_characteristic->disconnect();
}

void PingBLEService::didConnect()
{
	_serverConnected = true;
}

void PingBLEService::didReceiveData(uint8_t *data, size_t length)
{
    if (data[0] == 1) {
    	IOControl::sharedInstance()->setPeerIsOn(1);
    } else if (data[0] == 0) {
    	IOControl::sharedInstance()->setPeerIsOn(0);
    }
}

bool PingBLEService::isConnected() {
	return _serverConnected;
}

void PingBLEService::sendAck(bool success) {
//	uint8_t data[1] = {0};
//	if (success)
//		sendData(data, 0, messageTypeAck, _sequence);
//	else
//		sendData(data, 0, messageTypeNack, _sequence);
}

void PingBLEService::loop() {

//	if (_command.length() > 0) {
//	    if (Debug::sharedInstance()->processDebugCommand(_command)) {
//		    sendAck(true);
//	    } else {
//		    sendAck(false);
//	    }
//	    _command = "";
//	} else if (_sendAck) {
//	    sendAck(true);
//	    _sendAck = false;
//	} else if (_sendNack) {
//	    sendAck(false);
//	    _sendNack = false;
//	}
}
